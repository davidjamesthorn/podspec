Pod::Spec.new do |s|
  s.name             = 'EventDrivenProtocols'
  s.version          = '0.1.0.2'
  s.summary          = 'A short description of EventDrivenProtocols.'

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://bitbucket.org/davidthorn/eventdrivenprotocols.git'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'David Thorn' => 'david.thorn@atino.de' }
  s.source           = { :git => 'https://bitbucket.org/davidthorn/eventdrivenprotocols.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'

  s.source_files = 'EventDrivenProtocols/Classes/**/*'
  

end
