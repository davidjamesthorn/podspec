Pod::Spec.new do |s|
  s.name             = 'EventDrivenViewController'
  s.version          = '0.1.0.1'
  s.summary          = 'A short description of EventDrivenViewController.'


  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://bitbucket.org/davidthorn/eventdrivenviewcontroller.git'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'David Thorn' => 'david.thorn@atino.de' }
  s.source           = { :git => 'https://bitbucket.org/davidthorn/eventdrivenviewcontroller.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'

  s.source_files = 'EventDrivenViewController/Classes/**/*'
  
  s.resource_bundles = {
    'EventDrivenViewController' => ['EventDrivenViewController/Assets/*']
  }

s.dependency 'EventDrivenProtocols'
end
