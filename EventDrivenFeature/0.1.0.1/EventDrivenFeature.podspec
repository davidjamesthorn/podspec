
Pod::Spec.new do |s|
  s.name             = 'EventDrivenFeature'
  s.version          = '0.1.0.1'
  s.summary          = 'A short description of EventDrivenFeature.'


  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://bitbucket.org/davidthorn/eventdrivenfeature.git'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'David Thorn' => 'david.thorn@atino.de' }
  s.source           = { :git => 'https://bitbucket.org/davidthorn/eventdrivenfeature.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'

  s.source_files = 'EventDrivenFeature/Classes/**/*'
  



s.dependency 'VISPER'
s.dependency 'ATBundle'
s.dependency 'EventDrivenViewController'
s.dependency 'EventDrivenProtocols'

end
